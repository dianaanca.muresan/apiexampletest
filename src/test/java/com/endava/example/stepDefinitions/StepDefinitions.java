package com.endava.example.stepDefinitions;

import com.endava.example.bulider.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static org.junit.Assert.*;

public class StepDefinitions {

    private static Response getResponse;
    private static Response putResponse;
    private static String jsonString;
    private String getApiUrl = "https://reqres.in/api/users?page=2";
    private String putApiUrl = "https://reqres.in/api/users/2";

    @Given("the client has an GET API url")
    public void the_client_has_a_GET_API_url() {
        System.out.println("API url:" + getApiUrl);
    }

    @When("the client sends a GET request")
    public void the_client_sends_a_GET_request() throws Throwable {
        RequestSpecification request = RestAssured.given();
        getResponse = request.get(getApiUrl);
    }

    @Then("the client receives status code 200")
    public void the_client_receives_status_code_200() throws Throwable {
        int result = getResponse.getStatusCode();
        assertEquals(200, result);
    }

    @And("the response is displayed")
    public void the_response_is_displayed() {
        jsonString = getResponse.asString();
        System.out.println(jsonString);
    }

    // Scenario 2
    @Given("the client has an PUT API url")
    public void the_client_has_a_PUT_API_url() {
        System.out.println("API url: " + putApiUrl);
    }

    @When("the client updates a user")
    public void the_client_updates_a_user() throws JsonProcessingException {
        User.UserBuilder builder = new User.UserBuilder();
        ObjectMapper objectMapper = new ObjectMapper();

        builder.name("Diana").job("Tester");
        User user = builder.build();
        String json = objectMapper.writeValueAsString(user);

        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        request.body(json);
        putResponse = request.put(putApiUrl);

        jsonString = putResponse.asString();
        System.out.println(jsonString);
    }

    @Then("the client receives PUT status code 200")
    public void the_client_receives_PUT_status_code_200() {
        int result = putResponse.getStatusCode();
        assertEquals(200, result);
    }
}
