package com.endava.example.runners;

import io.cucumber.spring.CucumberContextConfiguration;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@CucumberContextConfiguration
@ContextConfiguration
@SpringBootTest(classes = SpringBootRunner.class)
public class SpringBootRunner {
}
