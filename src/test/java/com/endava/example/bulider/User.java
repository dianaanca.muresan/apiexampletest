package com.endava.example.bulider;

public class User {

    public static class UserBuilder {

//        private int id;
//        private String email;
//        private String firstName;
//        private String lastName;
//        private String avatar;
        private String name;
        private String job;

        public UserBuilder() {
        }

        public User build() {
            return new User(this);
        }

//        public UserBuilder id(int id) {
//            this.id = id;
//            return this;
//        }
//
//        public UserBuilder email(String email) {
//            this.email = email;
//            return this;
//        }
//
//        public UserBuilder firstName(String firstName) {
//            this.firstName = firstName;
//            return this;
//        }
//
//        public UserBuilder lastName(String lastName) {
//            this.lastName = lastName;
//            return this;
//        }
//
//        public UserBuilder avatar(String avatar) {
//            this.avatar = avatar;
//            return this;
//        }

        public UserBuilder name(String name) {
            this.name = name;
            return this;
        }

        public UserBuilder job(String job) {
            this.job = job;
            return this;
        }
    }

//    private final int id;
//    private final String email;
//    private final String firstName;
//    private final String lastName;
//    private final String avatar;
    private final String name;
    private final String job;

    private User(UserBuilder builder) {
//        this.id = builder.id;
//        this.email = builder.email;
//        this.firstName = builder.firstName;
//        this.lastName = builder.lastName;
//        this.avatar = builder.avatar;
        this.name = builder.name;
        this.job = builder.job;
    }

//    public int getId() {
//        return id;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public String getFirstName() {
//        return firstName;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public String getAvatar() {
//        return avatar;
//    }

    public String getName() {
        return name;
    }

    public String getJob() {
        return job;
    }
}
