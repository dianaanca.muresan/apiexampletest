Feature: manage users list

  Scenario: client makes a GET request
    Given the client has an GET API url
    When the client sends a GET request
    Then the client receives status code 200
    And the response is displayed

  Scenario: client makes a PUT request
    Given the client has an PUT API url
    When the client updates a user
    Then the client receives PUT status code 200
